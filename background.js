// Runs in the background when extension is activated.

/**
 * Uses Chrome Download API to save images locally
 * @param {Array<string>} imageUrls
 * TODO: download base64 encoded images.
 */
function downloadImages(imageUrls) {
  // NOTE: I've included a copy of the TM script used to download images for reference. I don't really understand the intent behind the regex matches and string interpolation this function is doing.
  // function saveImageUrls(urls) {
  //   // TODO
  //   for (const url of urls) {
  //     const result = /\/?([\w\-\.\s@]+)\??[\w\-\+=&\s\[\]\$\(\),;@:#!~'"]*$/i.exec(url);
  //     console.assert(result.length > 1, "Regex failed to math, aborting ...");
  //     const filename = result[1]; // capture group 1
  //     let dl_url = /^\/\//i.test(url)
  //       ? 'http:' + url
  //       : url;
  //     GM_download(dl_url, filename);
  //   }
  // }

  /////////////////////

  return Promise.allSettled(
    imageUrls.map((url) => chrome.downloads.download({ url }))
  );
}

/**
 * Content script dynamically injected when the extension icon is clicked.
 * Parses the page DOM for image source files passes them to service worker to
 * be downloaded.
 *
 * NOTE: This script logic was mostly copy-paste from the origin Tampermonkey
 * image download script. The main difference is the download action is handled
 * by the background service worker instead of Tampermonkey API calls.
 *
 * NOTE: Likely due to data serialization we are unable to return a Set object
 * from our content script to our background service worker. We are casting set
 * to a list first.
 */
function scanImages() {
  // HTML node attributes that to be checked for image source urls sorted by order of priority.
  const SRC_ATTRS = ["src", "data-src"];
  // File extensions checked against source URLs to determine if they should be downloaded.
  const ALLOWED_FILE_EXTS = [
    "png",
    "jpeg",
    "jpg",
    "bmp",
    "gif",
    "tiff",
    "webp",
  ];

  /****************************************************************************/

  console.log("Scanning DOM for image source URLs...");
  const imgNodes = document.getElementsByTagName("img");
  // Bailout if there are no images to download.
  if (imgNodes.length === 0) {
    console.log("No image elements found.");
    return [];
  }

  console.log("Extracting source URLs from image elements...");
  const regex = new RegExp(
    ALLOWED_FILE_EXTS.map((i) => `.${i}`).join("|"),
    "i"
  );

  // Using 'Set' to prevent duplicate image URLs from being downloaded.
  const imgUrls = new Set();
  for (const imgNode of imgNodes) {
    for (const attr of SRC_ATTRS) {
      const attrVal = imgNode.getAttribute(attr);
      if (regex.test(attrVal)) {
        imgUrls.add(attrVal);
        break; // Break out of inner loop since we found a image URL on a priority attribute.
      }
    }
  }

  console.log(`Found ${imgUrls.size} images.`, imgUrls);

  return [...imgUrls];
}

/**
 * Uses Chrome Notifications API to send an OS notifcation that image downloads
 * have completed. Provides QoL buttons in notification to open image download loaction.
 */
function notifyDownloadComplete() {
  return chrome.notifications.create({
    type: "basic",
    iconUrl: "images/alert.png", // IconURL is required
    title: "JWC Image Downloader",
    message: "Webpage images have finished downloading.",
    // TODO: When clicking "view images" should open the image download location.
    // buttons: [{ title: "View Images" }],
    priority: 0,
  });
}

// Add click event to extension icon to trigger download.
chrome.action.onClicked.addListener(function (tab) {
  // NOTE: Use network requests to discover images instead of parsing HTML DOM.
  // https://developer.chrome.com/docs/extensions/reference/webRequest/

  // Inject content script to parse DOM for image files.
  chrome.scripting
    .executeScript({
      target: { tabId: tab.id },
      func: scanImages,
    })
    .then((res) => {
      console.log("Results:", res);
      if (res.length === 0)
        return Promise.reject("No matching image URLs found.");

      // NOTE: 'res' could contain multiple frames - we are using only the top-level frame here.
      return res[0].result;
    })
    // NOTE: downloadImages returns an allSettled promise so any failed downloads must be checked from the promise results array in the promise chain.
    .then(downloadImages)
    .catch((err) => {
      console.error(err);
    })
    .finally(notifyDownloadComplete);
});
