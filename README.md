# JWC Image Downloader (Chrome Extension)

Enables one-click downloading of all images on the current webpage.

This extension is being developed as a custom solution for users which need
to easily download a large set of images from webpage, or circumvent any
obfuscation measures to prevent users from right-click downloading images.

Do not expect any updates beyond maintaining basic download functionality. Any feature-requests will likely be ignored or closed unless I find them useful
to myself or my clients.

**Client feature-requests should be made through official channels, not on this extensions issues page**.

## License

This project is licensed under the GPLv3 license. See [LICENSE](./LICENSE.txt)
for full terms.

Source files within this project repository are considered to implicitly include the following copyright license text unless they explicitly define their own copyright notice at the start of the file.

```
JWC Image Downloader (Chrome Extension) - Enables one-click downloading of all images on the current webpage.
Copyright (C) 2022  James Woods

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
